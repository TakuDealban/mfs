{{-- @extends('layouts.app', ['title' => __('Quotes Master')]) --}}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    {{-- <head> --}}
        {{-- <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="_base_url" content="{{ url('/') }}">

        <title>{{ config('app.name', 'Argon Dashboard') }}</title> --}}
        <!-- Favicon -->

        <link href="{{ asset('argon') }}/img/brand/favicon.png" rel="icon" type="image/png">
        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> --}}
        <!-- Icons -->
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link type="text/css" href="{{ asset('argon') }}/vendor/select2/select2.min.css" rel="stylesheet">
        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">

        <style>
             /* .table_in { overflow-x: visible !important; } */

            thead  {
  display: table-header-group;
}

tr {
    page-break-inside: avoid ;
}

table.table td {   padding-top: .2rem;   padding-bottom: .2rem;}

/* table {
    thead {
    display: table-header-group;
    break-inside: avoid;
    page-break-inside: avoid;
    }
} */


            </style>


    {{-- </head> --}}

    {{-- <body style="background:none"> --}}
{{-- @section('content') --}}
    {{-- @include('users.partials.header', ['title' => __('Quote INFO')]) --}}

    {{-- <div class="container-fluid mt--7"   > --}}
        {{-- <div class="row"> --}}
            {{-- <div class="col-xs-12"> --}}
                {{-- <div class="card bg-secondary shadow"> --}}
                    {{-- <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h1 class="mb-0">{{ __('QUOTE') }}</h1>
                            </div>

                        </div>
                    </div> --}}


                    <div class="card-body" style="background-color:white">

                            {{-- <div class="card-body">
                                          <div class="">
                                                <div class="row align-items-center">
                                                        <div class="col-8 text-center">
                                                            <h1 class="mb-0">{{ __('QUOTE') }}</h1>
                                                        </div>

                                                    </div>
                                          </div></div> --}}
                                                {{-- <div class="table-responsive"> --}}

                                                    <table class="table align-items-center">
                                                        <tr>
                                                            <td>
                                                                    <h1 class="mb-0 text-center">{{ __('QUOTATION') }}</h1>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                        <table class="table align-items-center">
                                                                <tr>
                                                                        <td>
                                        <div class="media align-items-center">
                               <img alt="Image placeholder" class="img-responsive" style="width:150px; height:150px; border:none" src="{{ asset('argon') }}/img/theme/mfs.jpg">
                                            </div>
                                        </td>

                                        <td>
                                            <span class="h3 font-weight-bold mb-0 pull-right">MFS GROUP PVT LTD</span><br>
                                            {!!$store_info->ADDRESS1 == NULL ? '' : '<span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS1.'</span><br>'!!}
                                            {!!$store_info->ADDRESS2 == NULL ? '' : '<span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS2.'</span><br>'!!}
                                           {!!$store_info->ADDRESS3 == NULL ? '' : ' <span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS3.'</span><br>'!!}
                                        <span class="h3 font-weight-bold mb-0 pull-right">Vat No: {{config('app.CO_VAT')}}</span><br>
                                                                                     </td>
                                                </tr>
                                                </table>
                                                    <table class="table align-items-center">
                                                        <tr>

                                                                    <td style="width:60%">
                                                                            <address>
                                                                <span class="h2  mb-0 push-5">{{$Quote_header->CUSTOMERNAME}}</span><br>

                                                                {!!trim($Quote_header->ADDRESS1) == trim("No-Addr1") ? '' : ' <span class="h4  mb-0">'.$Quote_header->ADDRESS1.'</span><br>'!!}
                                                                {!!trim($Quote_header->ADDRESS2) == trim("No-Addr2") ? '' : '<span class="h4  mb-0">'.$Quote_header->ADDRESS2.'</span><br>'!!}
                                                                {!!trim($Quote_header->ADDRESS3) == trim("No-Addr3") ? '' : ' <span class="h4  mb-0">'.$Quote_header->ADDRESS3.'</span><br>'!!}


                                                                            <span class="h2  mb-0">VAT: {{$Quote_header->VATNUMBER==NULL?"No Vat":$Quote_header->VATNUMBER}} </span>
                                                                </address>
                                                            </td>

                                                            <td>

                                                                                <address>
                                                                                    <span class="h2  mb-0 push-5">Quote No</span><br>
                                                                                    <span class="h2  mb-0">Quote Date  </span><br>
                                                                                    <span class="h2  mb-0">Print Date </span><br>
                                                                                    <span class="h2  mb-0">Sales Person</span><br>
                                                                                </address>
                                                                            </td>

                                                                            <td>
                                                                                        <address>
                                                                                            <span class="h2  mb-0 push-5">:</span><br>
                                                                                            <span class="h2  mb-0">:  </span><br>
                                                                                            <span class="h2  mb-0">:  </span><br>
                                                                                            <span class="h2  mb-0">:  </span><br>
                                                                                        </address>
                                                                                    </td>

                                                                                <td>
                                                                                        <address>
                                                                                            <span class="h2  mb-0 push-5">{{$Quote_header->QUOTENO}}</span><br>
                                                                                            <span class="h2  mb-0">{{date("d M y, h:ia",strtotime($Quote_header->created_at))}} </span><br>
                                                                                            <span class="h2  mb-0">{{$Quote_header->current_print_date == null ? date("d M y, h:ia") : date("d M y, h:ia",strtotime($Quote_header->current_print_date))}} </span><br>
                                                                                            <span class="h2  mb-0">{{auth()->user()->name}} </span><br>
                                                                                        </address>
                                                                                </td>
                                                                            </tr>
                                                                            </table>

                                                                            {{-- <div class="card-body">
                                                                            <div class="card card-stats mb-4 mb-lg-0 table_in"> --}}
                                                                            <table class="table align-items-center table-striped">
                                                                                    <thead class="thead-dark">
                                                                                        <tr>
                                                                                            <th scope="col" ><span class="font-weight-bold h4 text-white">Code</span></th>
                                                                                            <th scope="col"><span class="font-weight-bold h4 text-white">Description</span></th>
                                                                                            <th scope="col"><span class="font-weight-bold h4 text-white">Till Desc</span></th>
                                                                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Qty</span></th>
                                                                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Price (Inc)</span></th>
                                                                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Vat</span></th>
                                                                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Total</span></th>

                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody class="thead-light">

                                                                                        @foreach ($Quote_items as $item)

                                                                                        <tr>
                                                                                        <td>{{$item->USERCODE}}</td>
                                                                                        <td>{{$item->hasProd->NAME}}</td>
                                                                                        <td>{{$item->hasProd->TILLDESC}}</td>
                                                                                        <td class="text-right">{{number_format($item->ORDERQTY,2)}}</td>
                                                                                        <td class="text-right">{{number_format($item->PRICE,2)}}</td>
                                                                                        <td class="text-right">{{number_format($item->LINEVAT,2)}}</td>
                                                                                        <td class="text-right">{{number_format($item->LINETOTAL,2)}}</td>
                                                                                        </tr>
                                                                                        @endforeach


                                                                                                                                   </tbody>


                                                                                </table>

                                                                                {{-- summary --}}

                                                                                <table class="table align-items-left table-borderless">

                                                                                        <tr>
                                                                                            <td>
                                                                                                        <table class="table align-items-left" style=" background-color:white">

                                                                                                            <thead class="thead-dark">
                                                                                                                <tr>
                                                                                                                    <th scope="col" ><span class="font-weight-bold h4 text-white">Special Notes and Instructions</span></th>

                                                                                                                </tr>

                                                                                                                <tr>
                                                                                                                    <td scope="col" class="font-w600 text-left"><span>
                                                                                                                            <ul style="list-style-type:disc;">
                                                                                                                                    <li>
                                                                                                                                        <strong class="h2">PRICES SUBJECT TO CHANGE WITHOUT NOTICE</strong></li>

                                                                                                                                  </ul>
                                                                                                                                <span></td>

                                                                                                                </tr>

                                                                                                                <tr>
                                                                                                                        <td scope="col" class="font-w600 text-left"><strong class="h2">Thank You For Your Business </strong> <br>
                                                                                                                            <strong class="h3">PLEASE CONFIRM PRICES AND PRODUCT AVAILABILITY BEFORE MAKING PAYMENT. </strong> </td>

                                                                                                                    </tr>
                                                                                                            </thead>

                                                                                                                {{-- <tbody class="thead-dark">
                                                                                                                        <tr>
                                                                                                                                <td scope="col" class="font-w600 text-left"><span>
                                                                                                                                        <ul style="list-style-type:disc;">
                                                                                                                                                <li>
                                                                                                                                                    <strong class="h2">PRICES SUBJECT TO CHANGE WITHOUT NOTICE</strong></li>

                                                                                                                                              </ul>
                                                                                                                                            <span></td>

                                                                                                                            </tr>

                                                                                                                            <tr>
                                                                                                                                    <td scope="col" class="font-w600 text-left"></td>

                                                                                                                                </tr>






                                                                                                                </tbody> --}}


                                                                                                            </table>
                                                                                                        </td>

                                                                                            <td>
                                                                                        <table class="table " style=" background-color:white">

                                                                                            <tbody class="thead-light">
                                                                                                    <tr>
                                                                                                            <td scope="col" class="font-w600 text-right"></td>
                                                                                                            <td scope="col" class=" text-right" colspan="3"><strong class="h4">SUB TOTAL</strong></td>
                                                                                                        <td class="text-right" colspan="1"><strong>{{number_format($subtotal,2)}}</strong></td>

                                                                                                        </tr>

                                                                                                        <tr>
                                                                                                                <td scope="col" class="font-w600 text-right"></td>
                                                                                                                <td scope="col" class=" text-right" colspan="3"><strong class="h4">VAT TOTAL</strong></td>
                                                                                                            <td class="text-right"><strong>{{number_format($quote_vat_total,2)}}</strong></td>

                                                                                                            </tr>

                                                                                                            <tr>
                                                                                                                    <td scope="col" class="font-w600 text-right"></td>
                                                                                                                    <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h3">TOTAL</strong></td>
                                                                                                                <td class="text-right"><strong class="h3">{{number_format($quote_total,2)}}</strong></td>

                                                                                                                </tr>


                                                                                            </tbody>


                                                                                        </table>
                                                                                            </td>
                                                                                        </tr>

                                                                                    </table>


                                                                                    {{-- <table class="table align-items-left col-md-12">

                                                                                            <tbody>
                                                                                                    <tr>
                                                                                                            <td></td>
                                                                                                            <td scope="col" class="text-center"><strong class="font-weight-bold h4 text-black">Thank You For Your Business</strong></td>

                                                                                                        </tr>
                                                                                                <tr>
                                                                                                        <td scope="col" class="text-left" style=""></td>
                                                                                                        <td scope="col" class="text-left"><strong class="h2 text-black">PLEASE CONFIRM PRICES AND PRODUCT AVAILABILITY BEFORE MAKING PAYMENT. </strong></td>

                                                                                                    </tr>
                                                                                            </tbody>

                                                                                 </table> --}}
                                                                            {{-- </div>
                                                                            </div> --}}

                                                                        {{-- </div> --}}


                                                {{-- <div class="card-body">
                            <div class="table-responsive card card-stats mb-4 mb-lg-0">

                            </div>
                            </div> --}}

                            {{-- end of total --}}

                            {{-- beggining of narration --}}

                            {{-- end of narration --}}




                        {{-- </form> --}}
                        </div>
                    </div>
                {{-- </div>
            </div>
        </div> --}}

        {{-- @include('layouts.footers.auth') --}}
    {{-- </div> --}}

