  @extends('layouts.app', ['title' => __('Quotes Master')])

@section('content')
    @include('users.partials.header', ['title' => __('New Quote')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('New Quote') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('quotes.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        {{-- <form method="post" action="{{ route('user.store') }}">
                            @csrf --}}

                            {{-- customer info --}}
                            <h6 class="heading-small mb-4">{{ __('Customer Information') }}</h6>

                            <div class="row">
                                <div class="col-md-4">
                                  <div class="form-group">
                                        <label for="unitPrice" class="form-control-label">Customer Details</label>
                                    <div class="input-group mb-4">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                      </div>
                                      <input class="form-control typeahead" placeholder="Enter Customer Name or Customer Number" id="customerData" type="text">
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-8">
                                        <div class="form-group">
                                              <label for="unitPrice" class="form-control-label">Customer Address : <em class="text-danger">Split with comma. eg - 29 2nd Street Mutare, Mutare, Zimbawe</em></label>
                                          <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                            </div>
                                            <input class="form-control " placeholder="Customer Address" id="customerAddr" type="text">
                                          </div>
                                        </div>
                                      </div>

                              </div>

                              <div class="row">
                                    <div class="col-md-4">
                                      <div class="form-group">
                                            <label for="unitPrice" class="form-control-label">Customer Number</label>
                                        <div class="input-group mb-4">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                          </div>
                                          <input class="form-control " placeholder="Customer Number" disabled id="customerNumero" type="text">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                            <label for="unitPrice" class="form-control-label">Customer Name</label>
                                        <div class="input-group mb-4">
                                          <input class="form-control" placeholder="Customer Name" id="custName" type="text">
                                          <div class="input-group-append">
                                            <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-md-4">
                                            <div class="form-group">
                                                  <label for="unitPrice" class="form-control-label">Customer Vat No:</label>
                                              <div class="input-group mb-4">
                                                <input class="form-control" placeholder="Customer VAT No" id="custVatNo" type="text">
                                                <div class="input-group-append">
                                                  <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                  </div>



                                {{-- Product info --}}
                            <h6 class="heading-small mb-4">{{ __('Product Information') }}</h6>

                            <div class="row">

                              <div class="col-md-12">
                                <div class="form-group">
                                      <label for="unitPrice" class="form-control-label">Product Num</label>
                                  <div class="input-group mb-4">
                        <select name="product_id" id="product_select" class="form-control select2"></select>
                        <input type="hidden" id="prod_desc">
                        <input type="hidden" id="prod_code">
                        <input type="hidden" id="taxcode">
                        <input type="hidden" id="curr_id">
                        <input type="hidden" id="till_desc">
                                  </div>
                                </div>
                            </div>

                            </div>
                            <div class="row">
                                {{-- <div class="col-md-4">
                                  <div class="form-group">
                                        <label for="unitPrice" class="form-control-label">Product Listing</label>
                                    <div class="input-group mb-4">
                                        <select class="form-control" name="prodSku" id='selProduct' data-select-search="true">
                                            <option value='0'> Select Product </option>
                                            @foreach ($products as $item)
                                            @php
                                                $stock_on_hand = $item->STOCKONHAND - $item->STOCKALLOC;
                                            @endphp
                                            <option value="{{$item->USERCODE}}" data-proddesc="{{$item->NAME}}"
                                                        data-prodcode = "{{$item->USERCODE}}"
                                                        data-price="{{$item->SELLPRICE}}"
                                                        data-instock="{{$stock_on_hand}}"
                                                    data-taxcode="{{$item->hasVatCode->TAXID}}"
                                                    data-till_desc="{{$item->TILLDESC}}"
                                                >
                                                        {{$item->NAME}} - ({{$item->USERCODE}})
                                                    </option>
                                            @endforeach


                                          </select>



                                    </div>
                                  </div>
                                </div> --}}
                                {{-- end of product --}}

                                {{-- end of test product --}}

                                <div class="col-md-3">
                                    <div class="form-group">
                                            <label for="unitPrice" class="form-control-label">Unit Price (Inc Vat)</label>
                                      <div class="input-group mb-4">

                                        <input class="form-control" placeholder="Unit Price" id="unitPrice" type="number"  readonly>
                                        <div class="input-group-append">
                                          <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                <div class="col-md-4">
                                  <div class="form-group">
                                        <label for="unitPrice" class="form-control-label">In Stock</label>
                                    <div class="input-group mb-4">
                                      <input class="form-control" placeholder="In Stock" id="inStock" type="text"  disabled>
                                      <div class="input-group-append">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                            <label for="unitPrice" class="form-control-label">Order Quantity</label>
                                      <div class="input-group mb-4">
                                        <input class="form-control" id="order_qty" placeholder="Enter Order Quantity" type="number" step="0.01">
                                        <div class="input-group-append">
                                          <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-md-1">
                                    <div class="form-group">
                                            <label for="unitPrice" class="form-control-label"><span class="text-white">.</span></label>
                                            <div class="input-group mb-4">
                                        <button class="btn btn-icon btn-2 btn-primary" id="btn_add_new_quote_line" type="button" data-toggle="tooltip" title="add to order list">
                                            <span class="btn-inner--icon"><i class="ni ni-fat-add"></i></span>
                                        </div>
                                        </button>
                                    </div>
                                  </div>

                              </div>


                               {{-- jquery upload products on button click --}}
                               <div id="prodUploadContentTbl">
                            <h6 class="heading-small mb-4">{{ __('Uploaded Summary') }}</h6>

                            <div class="table-responsive">
                                    <table class="table align-items-center table-dark uploadProds">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col">Code</th>
                                            <th scope="col">Description</th>
                                            <th scope="col">Till Desc</th>
                                            <th scope="col" class="text-right">Qty</th>
                                            <th scope="col" class="text-right">Price (Inc)</th>
                                            <th scope="col" class="text-right">Vat</th>
                                            <th scope="col" class="text-right">Total</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbody_cls">



                                    </tbody>

                                    <tfoot class="thead-light">
                                            <tr>
                                                <th scope="col"></th>
                                                <th class="text-black"><strong class="h3">TOTALS</strong></th>
                                                <th scope="col" class="text-right"><span class="qty_to"></span></th>
                                                <th scope="col" class="text-right"></th>
                                                <th scope="col" class="text-right"></th>
                                                <th scope="col" class="text-right"><span class="vat_tot"></span></th>
                                                <th scope="col" class="text-right"><span class="quote_tot"></span></th>
                                                <th scope="col"></th>
                                            </tr>
                                        </tfoot>
                                </table>

                                </div>
                            </div>

                              {{-- End of table listing for new products --}}
                              <br>
                              <div class="row">


                                    <div class="col-md-8">
                                            <div class="form-group response">

                                            </div>
                                    </div>
                              </div>
<br>
                              <div class="row">


                                    <div class="col-md-1">
                                            <div class="form-group">
                                    <button class="btn btn-icon btn-3 btn-primary btn_create_quote" type="button">
                                            <span class="btn-inner--icon"><i class="ni ni-send"></i></span>

                                            <span class="btn-inner--text">Create Quote</span>

                                        </button>
                                            </div>
                                    </div>
                              </div>

                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')

 <script src="{{ asset('argon') }}/customScripts/quotes.products.js"></script>
 <script src="{{ asset('argon') }}/customScripts/quotes.customers.js"></script>
 <script src="{{ asset('argon') }}/customScripts/quotes.post.js"></script>


    @endpush

