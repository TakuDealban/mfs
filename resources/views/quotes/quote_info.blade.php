@extends('layouts.app', ['title' => __('Quotes Master')])

@section('content')
    @include('users.partials.header', ['title' => __('QUOTATION INFO')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('QUOTATION Info') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                    <a href="{{ route('quotes.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                                <a href="{{config('app.url')}}/mfs/public/pdfview/{{$Quote_header->QUOTENO}}" target="_blank" class="btn btn-sm btn-primary">{{ __('Print Quote') }}</a>
                            </div>
                        </div>
                    </div>


                    <div class="card-body">
                        {{-- <form method="post" action="{{ route('user.store') }}">
                            @csrf --}}

                            <div class="row">
                                <div class="col-md-6">
                                        <div class="card card-stats mb-4 mb-lg-0">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">

                                    <div class="media align-items-center">

                                                <img alt="Image placeholder" class="img-responsive" style="width:150px; height:150px; border:none" src="{{ asset('argon') }}/img/theme/mfs.jpg">


                                        </div>
                                                        </div>

                                                        <div class="col-md-6 ml-auto pl-2">

                                                        <span class="h3 font-weight-bold mb-0 pull-right">MFS GROUP PVT LTD</span><br>
                                                                {!!$store_info->ADDRESS1 == NULL ? '' : '<span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS1.'</span><br>'!!}
                                                                {!!$store_info->ADDRESS2 == NULL ? '' : '<span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS2.'</span><br>'!!}
                                                               {!!$store_info->ADDRESS3 == NULL ? '' : ' <span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS3.'</span><br>'!!}
                                                                <span class="h3 font-weight-bold mb-0 pull-right">Vat No: {{config('app.CO_VAT')}}</span><br>
                                                            </div>

                                                    </div>

                                                </div>
                                            </div>
                                </div>



                              </div>
                            <br>

                            <div class="row">
                                    <div class="col-md-12">
                                         <span class="h1 font-weight-bold mb-0">Customer Details</span>

                                    </div>
                         </div>

                                  <br>
                              <div class="row">
                                    <div class="col-md-6">
                                            <div class="card card-stats mb-4 mb-lg-0">
                                                    <div class="card-body">
                                                        <div class="row">
                                                          <div class="col-md-6">
                                                                <address>
                                                                <span class="h4  mb-0 push-5">{{$Quote_header->CUSTOMERNAME}}</span><br>

                                                                   {!!trim($Quote_header->ADDRESS1) == trim("No-Addr1") ? '' : ' <span class="h4  mb-0">'.$Quote_header->ADDRESS1.'</span><br>'!!}
                                                                   {!!trim($Quote_header->ADDRESS2) == trim("No-Addr2") ? '' : '<span class="h4  mb-0">'.$Quote_header->ADDRESS2.'</span><br>'!!}
                                                                   {!!trim($Quote_header->ADDRESS3) == trim("No-Addr3") ? '' : ' <span class="h4  mb-0">'.$Quote_header->ADDRESS3.'</span><br>'!!}

                                                                            <span class="h4  mb-0">VAT: {{$Quote_header->VATNUMBER==NULL?"No Vat":$Quote_header->VATNUMBER}} </span>
                                                                            <address>
                                                                </div>


                                                        </div>

                                                    </div>
                                                </div>
                                    </div>

                                    <div class="col-md-6">
                                            <div class="card card-stats mb-4 mb-lg-0">
                                                    <div class="card-body">
                                                        <div class="row">
                                                                <div class="col-md-3">
                                                                    <address>
                                                                        <span class="h4  mb-0 push-5">Quote No</span><br>
                                                                        <span class="h4  mb-0">Quote Date  </span><br>
                                                                        <span class="h4  mb-0">Print Date </span><br>
                                                                        <span class="h4  mb-0">Sales Person</span><br>
                                                                    </address>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                            <address>
                                                                                <span class="h4  mb-0 push-5">:</span><br>
                                                                                <span class="h4  mb-0">:  </span><br>
                                                                                <span class="h4  mb-0">:  </span><br>
                                                                                <span class="h4  mb-0">:  </span><br>
                                                                            </address>
                                                                            </div>

                                                                    <div class="col-md-4 ">
                                                                            <address>
                                                                                <span class="h4  mb-0 push-5">{{$Quote_header->QUOTENO}}</span><br>
                                                                                <span class="h4  mb-0">{{date("d M y, h:ia",strtotime($Quote_header->created_at))}} </span><br>
                                                                                <span class="h4  mb-0">{{$Quote_header->current_print_date == null ? date("d M y, h:ia") : date("d M y, h:ia",strtotime($Quote_header->current_print_date))}} </span><br>
                                                                                <span class="h4  mb-0">{{auth()->user()->name}} </span><br><br>
                                                                            </address>
                                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                          </div>

                                  </div>

                                  <br>

                                  <div class="row">
                                        <div class="col-md-12">
                                             <span class="h4 font-weight-bold mb-0">Sales Items</span>

                                        </div>
                             </div>
                            <br>

                            <div class="table-responsive">
                                    <table class="table align-items-center table-striped table-hover">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col" ><span class="font-weight-bold h4 text-white">Code</span></th>
                                            <th scope="col"><span class="font-weight-bold h4 text-white">Description</span></th>
                                            <th scope="col"><span class="font-weight-bold h4 text-white">Till Desc</span></th>
                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Qty</span></th>
                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Price (Inc)</span></th>
                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Vat</span></th>
                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Total</span></th>

                                        </tr>
                                    </thead>
                                    <tbody class="thead-light">

                                        @foreach ($Quote_items as $item)

                                        <tr>
                                        <td>{{$item->USERCODE}}</td>
                                        <td>{{$item->hasProd->NAME}}</td>
                                        <td>{{$item->hasProd->TILLDESC}}</td>
                                        <td class="text-right">{{number_format($item->ORDERQTY,2)}}</td>
                                        <td class="text-right">{{number_format($item->PRICE,2)}}</td>
                                        <td class="text-right">{{number_format($item->LINEVAT,2)}}</td>
                                        <td class="text-right">{{number_format($item->LINETOTAL,2)}}</td>
                                        </tr>
                                        @endforeach


                                                                                   </tbody>


                                </table>
                            </div>
                                <div class="row">
                                    {{-- quote notes --}}
                                        <div class="col-md-6">
                                                <table class="table align-items-center table-borderless">

                                                        <tbody class="thead-light">
                                                                <tr>
                                                                        <td scope="col" class="font-w600 text-left"><strong class="h2">Special Notes and Instructions</strong> <br><span>
                                                                                <ul style="list-style-type:disc;">
                                                                                        <li><em>PRICES SUBECT TO CHANGE WITHOUT NOTICE</em></li>

                                                                                      </ul>
                                                                                    <span></td>

                                                                    </tr>

                                                                    <tr>
                                                                            <td scope="col" class="font-w600 text-left"></td>

                                                                        </tr>




                                                        </tbody>


                                                    </table>
                                        </div>
                                        {{-- start of totals  --}}
                                    <div class="col-md-6">
                                <table class="table align-items-center table-borderless">

                                    <tbody class="thead-light">
                                            <tr>
                                                    <td scope="col" class="font-w600 text-right"></td>
                                                    <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">SUB TOTAL</strong></td>
                                                <td class="text-right"><strong>{{number_format($subtotal,2)}}</strong></td>

                                                </tr>

                                                <tr>
                                                        <td scope="col" class="font-w600 text-right"></td>
                                                        <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">VAT TOTAL</strong></td>
                                                    <td class="text-right"><strong>{{number_format($quote_vat_total,2)}}</strong></td>

                                                    </tr>

                                                    <tr>
                                                            <td scope="col" class="font-w600 text-right"></td>
                                                            <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">TOTAL</strong></td>
                                                        <td class="text-right"><strong>{{number_format($quote_total,2)}}</strong></td>

                                                        </tr>


                                    </tbody>


                                </table>
                                    </div></div>





                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')

 <script src="{{ asset('argon') }}/customScripts/quotes.products.js"></script>
 <script src="{{ asset('argon') }}/customScripts/quotes.customers.js"></script>
 <script src="{{ asset('argon') }}/customScripts/quotes.post.js"></script>

    @endpush

