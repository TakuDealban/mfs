@extends('layouts.app', ['title' => __('Quotes Master')])

@section('content')
    @include('layouts.headers.cards')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                            <h3 class="mb-0">{{ __('Quotations') }} ({{count($Quotes)}})</h3>
                            </div>
                            <div class="col-4 text-right">
                                <a href="{{ route('quotes.create') }}" class="btn btn-sm btn-primary">{{ __('New Quote') }}</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush" id="table_quotes">

                            <thead class="thead-dark">
                                <tr>
                                        <th scope="col"></th>
                                    <th scope="col">{{ __('Quote Ref') }}</th>
                                    <th scope="col">{{ __('Customer Number') }}</th>
                                    <th scope="col">{{ __('Customer Name') }}</th>
                                    <th scope="col">{{ __('Quote Total') }}</th>
                                    <th scope="col">{{ __('VAT Total') }}</th>
                                    <th scope="col">{{ __('Quote Status') }}</th>
                                    <th scope="col">{{ __('Re-Prints') }}</th>
                                    <th scope="col">{{ __('Creation Date') }}</th>
                                    {{-- <th scope="col">{{ __('Last Print Date') }}</th> --}}




                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($Quotes as $quote)
                                    <tr>
                                            <td class="text-right">
                                                    <a class="text-info" data-toggle="tooltip" title="View Quote Info" href="{{ route('quotes.show', $quote->QUOTENO) }}"><i class="ni ni-like-2"></i></a>
                                            </td>
                                        <td>{{ $quote->QUOTENO }}</td>
                                        {{-- <td>
                                            <a href="mailto:{{ $quote->email }}">{{ $quote->email }}</a>
                                        </td> --}}
                                        <td>{{ $quote->CUSTOMERNUMBER }}</td>
                                        <td>{{ $quote->CUSTOMERNAME }}</td>
                                        <td class="text-right">{{ number_format($quote->QOUTETOTAL,2) }}</td>
                                        <td class="text-right">{{ number_format($quote->VATTOTAL,2) }}</td>
                                        <td>{{ $quote->QUOTESTATUS }}</td>

                                        {{-- <td>{{ date("d M y",strtotime($quote->current_print_date)) }}</td> --}}
                                        <td>{{ $quote->print_number == null ? 0 : $quote->print_number }}</td>
                                        <td>{{ date("d M y",strtotime($quote->CREATEDDATE)) }}</td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            {{-- {{ $Quotes->links() }} --}}
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
<script>
        $(document).ready(function() {
          $('#table_quotes').DataTable();
      } );
       </script>
@endpush
