{{-- @extends('layouts.app', ['title' => __('Quotes Master')]) --}}

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    {{-- <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="_base_url" content="{{ url('/') }}">

        <title>{{ config('app.name', 'Argon Dashboard') }}</title> --}}
        <!-- Favicon -->

        <link href="{{ asset('argon') }}/img/brand/favicon.png" rel="icon" type="image/png">
        <!-- Fonts -->
        {{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"> --}}
        <!-- Icons -->
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <link type="text/css" href="{{ asset('argon') }}/vendor/select2/select2.min.css" rel="stylesheet">
        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">

        <style>
            /* .table_in { overflow-x: visible !important; } */

/*

            body {
  overflow-wrap: break-word;
  word-wrap: break-word;
  -webkit-hyphens: auto;
  -ms-hyphens: auto;
  -moz-hyphens: auto;
  hyphens: auto;
} */


thead  {
  display: table-header-group;
}

tr {
    page-break-inside: avoid ;
}
/* https://mdbootstrap.com/support/jquery/make-table-row-height-font-and-checkboxes-smaller/ */
table.table td {   padding-top: .2rem;   padding-bottom: .2rem;}

            </style>


    {{-- </head>

    <body style="background:none"> --}}
{{-- @section('content') --}}
    {{-- @include('users.partials.header', ['title' => __('Invoice INFO')]) --}}

    {{-- <div class="container-fluid mt--7"   > --}}
        {{-- <div class="row">
            <div class="col-xs-12">
                <div class="card bg-secondary shadow"> --}}
                    {{-- <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h1 class="mb-0">{{ __('INVOICE - COPY') }}</h1>
                            </div>

                        </div>
                    </div> --}}


                    <div class="card-body" style="background-color:white">

                            {{-- <div class="card-body">
                                          <div class="">
                                                <div class="row align-items-center">
                                                        <div class="col-8 text-center">
                                                            <h1 class="mb-0">{{ __('QUOTE') }}</h1>
                                                        </div>

                                                    </div>
                                          </div></div> --}}
                                                {{-- <div class="table-responsive"> --}}

                                                    <table class="table align-items-center">
                                                        <tr>
                                                            <td>
                                                                    <h1 class="mb-0 text-center">{{ __('TAX INVOICE - COPY') }}</h1>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                        <table class="table align-items-center">
                                                                <tr>
                                                                        <td>
                                        <div class="media align-items-center">
                               <img alt="Image placeholder" class="img-responsive" style="width:150px; height:150px; border:none" src="{{ asset('argon') }}/img/theme/mfs.jpg">
                                            </div>
                                        </td>

                                        <td>
                                            <span class="h3 font-weight-bold mb-0 pull-right">MFS GROUP PVT LTD</span><br>
                                            {!!$store_info->ADDRESS1 == NULL ? '' : '<span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS1.'</span><br>'!!}
                                            {!!$store_info->ADDRESS2 == NULL ? '' : '<span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS2.'</span><br>'!!}
                                           {!!$store_info->ADDRESS3 == NULL ? '' : ' <span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS3.'</span><br>'!!}
                                        <span class="h3 font-weight-bold mb-0 pull-right">Vat No: {{config('app.CO_VAT')}}</span><br>

                                                                                     </td>
                                                </tr>
                                                </table>
                                                    <table class="table align-items-center">
                                                        <tr>

                                                                    <td style="width:60%">
                                                                        <address>

                                                                            <span class="h4  mb-0 push-5">{{$customer_name}} ({{$customer_number}})</span><br>
                                                                            {!!trim($addr_one) == trim("No-Addr1") ? '' : ' <span class="h4  mb-0">'.$addr_one.'</span><br>'!!}
                                                                            {!!trim($addr_two) == trim("No-Addr2") ? '' : '<span class="h4  mb-0">'.$addr_two.'</span><br>'!!}
                                                                            {!!trim($addr_three) == trim("No-Addr3") ? '' : ' <span class="h4  mb-0">'.$addr_three.'</span><br>'!!}

                                                                                        <span class="h4  mb-0">VAT: {{$customer_vat}} </span>
                                                                                        <address>
                                                            </td>

                                                            <td>

                                                                <address>
                                                                    <span class="h4  mb-0 push-5">Invoice No</span><br>
                                                                    <span class="h4  mb-0">Invoice Date  </span><br>
                                                                    <span class="h4  mb-0">Order Number </span><br>
                                                                    <span class="h4  mb-0">Cashier Name</span><br>
                                                                </address>
                                                                            </td>

                                                                            <td>
                                                                                <address>
                                                                                    <span class="h4  mb-0 push-5">:</span><br>
                                                                                    <span class="h4  mb-0">:  </span><br>
                                                                                    <span class="h4  mb-0">:  </span><br>
                                                                                    <span class="h4  mb-0">:  </span><br>
                                                                                </address>
                                                                                    </td>

                                                                                <td>
                                                                                    <address>
                                                                                       <br> <span class="h4  mb-0 push-5">{{$invoice_header->TRANNO}}</span><br>
                                                                                        <span class="h4  mb-0">{{date("d M y",strtotime($invoice_header->TRANDATE))}} </span><br>
                                                                                        <span class="h4  mb-0">{{$order_number}} </span><br>
                                                                                        <span class="h4  mb-0">{{$invoice_header->hasCashName->NAME}} </span><br><br>
                                                                                    </address>
                                                                                </td>
                                                                            </tr>
                                                                            </table>

                                                                            {{-- <div class="card-body">
                                                                            <div class="card card-stats mb-4 mb-lg-0 table_in"> --}}
                                                                            <table class="table align-items-center table-striped table-sm" style="border: 0px;">
                                                                                    <thead class="thead-dark">
                                                                                        <tr>
                                                                                            <th scope="col" ><span class="font-weight-bold h4 text-white">Code</span></th>
                                                                                            <th scope="col"><span class="font-weight-bold h4 text-white">Description</span></th>
                                                                                            {{-- <th scope="col"><span class="font-weight-bold h4 text-white">Till Desc</span></th> --}}
                                                                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Qty</span></th>
                                                                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Price (Inc)</span></th>
                                                                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Vat</span></th>
                                                                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Total</span></th>

                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody class="thead-light">

                                                                                        @foreach ($invoice_details as $item)

                                                                            <tr >
                                                                            <td>{{$item->hasInvProd->USERCODE}}</td>
                                                                            <td>{{$item->hasInvProd->NAME}}</td>
                                                                            {{-- <td>{{$item->hasProd->TILLDESC}}</td> --}}
                                                                            <td class="text-right">{{number_format($item->QTY,2)}}</td>
                                                                            <td class="text-right">{{number_format($item->hasInvProd->SELLPRICE,2)}}</td>
                                                                            <td class="text-right">{{number_format($item->TAXVALUE,2)}}</td>
                                                                            <td class="text-right">{{number_format($item->SELLVALUE,2)}}</td>
                                                                            </tr>
                                                                            @endforeach

                                                                      </tbody>
                                                                                </table>

                                                                                {{-- summary --}}


                                                                                        <table class="table align-items-right" style=" background-color:white">

                                                                                            <tbody class="thead-light">
                                                                                                <tr>
                                                                                                    <td scope="col" class="font-w600 text-right"></td>
                                                                                                    <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">SUB TOTAL</strong></td>
                                                                                                <td class="text-right"><strong>{{number_format($invoice_header->TOTSALES - $vat_total,2)}}</strong></td>

                                                                                                </tr>

                                                                                                <tr>
                                                                                                        <td scope="col" class="font-w600 text-right"></td>
                                                                                                        <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">VAT TOTAL</strong></td>
                                                                                                    <td class="text-right"><strong>{{number_format($vat_total,2)}}</strong></td>

                                                                                                    </tr>

                                                                                                    <tr>
                                                                                                            <td scope="col" class="font-w600 text-right"></td>
                                                                                                            <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">TOTAL</strong></td>
                                                                                                        <td class="text-right"><strong>{{number_format($invoice_header->TOTSALES,2)}}</strong></td>

                                                                                                        </tr>


                                                                                            </tbody>


                                                                                        </table>

                                                                                  {{-- </div>
                                                                            </div> --}}

                                                                        {{-- </div> --}}


                                                {{-- <div class="card-body">
                            <div class="table-responsive card card-stats mb-4 mb-lg-0">

                            </div>
                            </div> --}}

                            {{-- end of total --}}

                            {{-- beggining of narration --}}

                            {{-- end of narration --}}




                        {{-- </form> --}}
                        </div>
                    {{-- </div>
                </div>
            </div> --}}
        {{-- </div> --}}

        {{-- @include('layouts.footers.auth') --}}

    {{-- </body> --}}
{{-- @endsection --}}
{{--
@push('js')

 <script src="{{ asset('argon') }}/customScripts/quotes.products.js"></script>
 <script src="{{ asset('argon') }}/customScripts/quotes.customers.js"></script>
 <script src="{{ asset('argon') }}/customScripts/quotes.post.js"></script>

    @endpush --}}

