@extends('layouts.app', ['title' => __('Invoice Master')])

@section('content')
    @include('users.partials.header', ['title' => __('Invoice INFO')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Invoice Info') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                    <a href="{{ route('invoice.search') }}" class="btn btn-sm btn-primary">{{ __('Back to Search') }}</a>
                            <a href="{{config('app.url')}}/mfs/public/invoice/print/inv_num/{{$invoice_header->TRANNO}}/cashier_no/{{$invoice_header->CASHIERNO}}/tran_date/{{$invoice_header->TRANDATE}}" target="_blank" class="btn btn-sm btn-primary">{{ __('Print Invoice') }}</a>
                            </div>
                        </div>
                    </div>


                    <div class="card-body">
                        {{-- <form method="post" action="{{ route('user.store') }}">
                            @csrf --}}

                            <div class="row">
                                <div class="col-md-6">
                                        <div class="card card-stats mb-4 mb-lg-0">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-md-6">

                                    <div class="media align-items-center">

                                                <img alt="Image placeholder" class="img-responsive" style="width:150px; height:150px; border:none" src="{{ asset('argon') }}/img/theme/mfs.jpg">


                                        </div>
                                                        </div>

                                                        <div class="col-md-6 ml-auto pl-2">

                                                            <span class="h3 font-weight-bold mb-0 pull-right">MFS GROUP PVT LTD</span><br>
                                                            {!!$store_info->ADDRESS1 == NULL ? '' : '<span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS1.'</span><br>'!!}
                                                            {!!$store_info->ADDRESS2 == NULL ? '' : '<span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS2.'</span><br>'!!}
                                                           {!!$store_info->ADDRESS3 == NULL ? '' : ' <span class="h3 font-weight-bold mb-0 pull-right">'.$store_info->ADDRESS3.'</span><br>'!!}
                                                        <span class="h3 font-weight-bold mb-0 pull-right">Vat No: {{config('app.CO_VAT')}}</span><br>
                                                       </div>

                                                    </div>

                                                </div>
                                            </div>
                                </div>



                              </div>
                            <br>

                            <div class="row">
                                    <div class="col-md-12">
                                         <span class="h1 font-weight-bold mb-0">Customer Details</span>

                                    </div>
                         </div>

                                  <br>
                              <div class="row">
                                    <div class="col-md-6">
                                            <div class="card card-stats mb-4 mb-lg-0">
                                                    <div class="card-body">
                                                        <div class="row">
                                                          <div class="col-md-6">
                                                                <address>

                                                                <span class="h4  mb-0 push-5">{{$customer_name}} ({{$customer_number}})</span><br>

                                                                    {!!trim($addr_one) == trim("No-Addr1") ? '' : ' <span class="h4  mb-0">'.$addr_one.'</span><br>'!!}
                                                                    {!!trim($addr_two) == trim("No-Addr2") ? '' : '<span class="h4  mb-0">'.$addr_two.'</span><br>'!!}
                                                                    {!!trim($addr_three) == trim("No-Addr3") ? '' : ' <span class="h4  mb-0">'.$addr_three.'</span><br>'!!}


                                                                            <span class="h4  mb-0">VAT: {{$customer_vat}} </span>
                                                                            <address>
                                                                </div>


                                                        </div>

                                                    </div>
                                                </div>
                                    </div>

                                    <div class="col-md-6">
                                            <div class="card card-stats mb-4 mb-lg-0">
                                                    <div class="card-body">
                                                        <div class="row">
                                                                <div class="col-md-5">
                                                                    <address>
                                                                        <span class="h4  mb-0 push-5">Invoice No</span><br>
                                                                        <span class="h4  mb-0">Invoice Date  </span><br>
                                                                        <span class="h4  mb-0">Order Number </span><br>
                                                                        <span class="h4  mb-0">Cashier Name</span><br>
                                                                    </address>
                                                                    </div>

                                                                    <div class="col-md-1">
                                                                            <address>
                                                                                <span class="h4  mb-0 push-5">:</span><br>
                                                                                <span class="h4  mb-0">:  </span><br>
                                                                                <span class="h4  mb-0">:  </span><br>
                                                                                <span class="h4  mb-0">:  </span><br>
                                                                            </address>
                                                                            </div>

                                                                    <div class="col-md-4 ">
                                                                            <address>
                                                                                <span class="h4  mb-0 push-5">{{$invoice_header->TRANNO}}</span><br>
                                                                                <span class="h4  mb-0">{{date("d M y",strtotime($invoice_header->TRANDATE))}} </span><br>
                                                                                <span class="h4  mb-0">{{$order_number}} </span><br>
                                                                                <span class="h4  mb-0">{{$invoice_header->hasCashName->NAME}} </span><br><br>
                                                                            </address>
                                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                          </div>

                                  </div>

                                  <br>

                                  <div class="row">
                                        <div class="col-md-12">
                                             <span class="h4 font-weight-bold mb-0">Sales Items</span>

                                        </div>
                             </div>
                            <br>

                            <div class="table-responsive">
                                    <table class="table align-items-center table-striped table-hover">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th scope="col" ><span class="font-weight-bold h4 text-white">Code</span></th>
                                            <th scope="col"><span class="font-weight-bold h4 text-white">Description</span></th>
                                            {{-- <th scope="col"><span class="font-weight-bold h4 text-white">Till Desc</span></th> --}}
                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Qty</span></th>
                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Price (Inc)</span></th>
                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Vat</span></th>
                                            <th scope="col" class="text-right"><span class="font-weight-bold h4 text-white">Total</span></th>

                                        </tr>
                                    </thead>
                                    <tbody class="thead-light">

                                        @foreach ($invoice_details as $item)

                                        <tr>
                                        <td >{{$item->hasInvProd->USERCODE}}</td>
                                        <td>{{$item->hasInvProd->NAME}}</td>
                                        {{-- <td>{{$item->hasProd->TILLDESC}}</td> --}}
                                        <td class="text-right">{{number_format($item->QTY,2)}}</td>
                                        <td class="text-right">{{number_format($item->hasInvProd->SELLPRICE,2)}}</td>
                                        <td class="text-right">{{number_format($item->TAXVALUE,2)}}</td>
                                        <td class="text-right">{{number_format($item->SELLVALUE,2)}}</td>
                                        </tr>
                                        @endforeach


                                                                                   </tbody>


                                </table>
                            </div>
                                <div class="row">
                                    {{-- quote notes --}}
                                        <div class="col-md-6">

                                        </div>
                                        {{-- start of totals  --}}
                                    <div class="col-md-6">
                                <table class="table align-items-center table-borderless">

                                    <tbody class="thead-light">
                                            <tr>
                                                    <td scope="col" class="font-w600 text-right"></td>
                                                    <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">SUB TOTAL</strong></td>
                                                <td class="text-right"><strong>{{number_format($invoice_header->TOTSALES - $vat_total,2)}}</strong></td>

                                                </tr>

                                                <tr>
                                                        <td scope="col" class="font-w600 text-right"></td>
                                                        <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">VAT TOTAL</strong></td>
                                                    <td class="text-right"><strong>{{number_format($vat_total,2)}}</strong></td>

                                                    </tr>

                                                    <tr>
                                                            <td scope="col" class="font-w600 text-right"></td>
                                                            <td scope="col" class="font-w600 text-right" colspan="3"><strong class="h4">TOTAL</strong></td>
                                                        <td class="text-right"><strong>{{number_format($invoice_header->TOTSALES,2)}}</strong></td>

                                                        </tr>


                                    </tbody>


                                </table>
                                    </div></div>





                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')

 {{-- <script src="{{ asset('argon') }}/customScripts/quotes.products.js"></script>
 <script src="{{ asset('argon') }}/customScripts/quotes.customers.js"></script>
 <script src="{{ asset('argon') }}/customScripts/quotes.post.js"></script> --}}

    @endpush

