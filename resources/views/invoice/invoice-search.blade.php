@extends('layouts.app', ['title' => __('Quotes Master')])

@section('content')
    @include('users.partials.header', ['title' => __('Invoice Search')])

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col-xl-12 order-xl-1">
                <div class="card bg-secondary shadow">
                    <div class="card-header bg-white border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                                <h3 class="mb-0">{{ __('Invoice Lookup') }}</h3>
                            </div>
                            <div class="col-4 text-right">
                                {{-- <a href="{{ route('quotes.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a> --}}
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="post" action="{{ route('invoice.post') }}" id="formSearch">
                            @csrf

                            {{-- customer info --}}
                            <h6 class="heading-small mb-4">{{ __('Search Header') }}</h6>

                            <div class="row">
                                <div class="col-md-4">
                                  <div class="form-group">
                                  <label for="" class="form-control-label"> {{__('Invoice Number')}}</label>
                                    <div class="input-group mb-4">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                      </div>
                                      <input class="form-control" name="invoice_number" required placeholder="Enter Invoice Number"  type="number">
                                    </div>
                                  </div>
                                </div>

                                                                <div class="col-md-4">
                                        <div class="form-group">
                                              <label for="" class="form-control-label">Invoice Date</label>
                                          <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                            </div>
                                            <input class="form-control " name="invoice_date" required placeholder="Pick Date"  type="date">
                                          </div>
                                        </div>
                                      </div>

                                      <div class="col-md-4">
                                        <div class="form-group">
                                              <label for="unitPrice" class="form-control-label">Cashier Number</label>
                                          <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                            </div>
                                            <input class="form-control " name="cashier_num" required placeholder="Enter Cashier Number"  id="cashierNum" type="number">
                                          </div>
                                        </div>
                                      </div>

                              </div>

                              <h6 class="heading-small mb-4">{{ __('Customer Information') }}</h6>

                            <div class="row">
                                <div class="col-md-4">
                                  <div class="form-group">
                                        <label for="unitPrice" class="form-control-label">Customer Details</label>
                                    <div class="input-group mb-4">
                                      <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                      </div>
                                      <input class="form-control typeahead" name="search_header" required placeholder="Enter Customer Name or Customer Number" id="customerData" type="text">
                                    </div>
                                  </div>
                                </div>

                                <div class="col-md-8">
                                        <div class="form-group">
                                              <label for="unitPrice" class="form-control-label">Customer Address : <em class="text-danger">Split with comma. eg - 29 2nd Street Mutare, Mutare, Zimbawe</em></label>
                                          <div class="input-group mb-4">
                                            <div class="input-group-prepend">
                                              <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                            </div>
                                            <input class="form-control" name="customer_addr" required placeholder="Customer Address" id="customerAddr" type="text">
                                          </div>
                                        </div>
                                      </div>

                              </div>

                              <div class="row">
                                    <div class="col-md-4">
                                      <div class="form-group">
                                            <label for="unitPrice" class="form-control-label">Customer Number</label>
                                        <div class="input-group mb-4">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                          </div>
                                          <input class="form-control " name="customer_number" required placeholder="Customer Number"  id="customerNumero" type="text">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="form-group">
                                            <label for="unitPrice" class="form-control-label">Customer Name</label>
                                        <div class="input-group mb-4">
                                          <input class="form-control" placeholder="Customer Name" name="customer_name" id="custName" type="text">
                                          <div class="input-group-append">
                                            <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-md-4">
                                            <div class="form-group">
                                                  <label for="unitPrice" class="form-control-label">Customer Vat No:</label>
                                              <div class="input-group mb-4">
                                                <input class="form-control" placeholder="Customer VAT No" name="customer_vat" id="custVatNo" type="text">
                                                <div class="input-group-append">
                                                  <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                  </div>

                                  <div class="row">
                                    <div class="col-md-4">
                                      <div class="form-group">
                                            <label for="unitPrice" class="form-control-label">Order Number (if available)</label>
                                        <div class="input-group mb-4">
                                          <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-zoom-split-in"></i></span>
                                          </div>
                                          <input class="form-control " name="order_number"  placeholder="Enter Order Number"  id="ordernumber" type="text">
                                        </div>
                                      </div>
                                    </div>
                                </div>

                              <div class="row">


                                <div class="col-6">
                                    @if (session('status'))
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            {{ session('status') }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                </div>

                              </div>

                              <div class="row">



                                    <div class="col-md-1">
                                        <div class="text-left">
                                            <button type="submit" class="btn btn-success mt-4">{{ __('Search Invoice') }}</button>
                                        </div>
                                    </div>
                              </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')


 <script src="{{ asset('argon') }}/customScripts/quotes.customers.js"></script>


    @endpush

