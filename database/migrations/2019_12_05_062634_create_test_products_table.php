<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_products', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('prodCode')->unique();
            $table->string('prodDesc')->nullable();
            $table->string('prod_till_desc')->nullable();
            $table->integer('prodQty')->nullable();
            $table->decimal('prodPrice',10,4)->nullable();
            $table->integer('taxid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_products');
    }
}
