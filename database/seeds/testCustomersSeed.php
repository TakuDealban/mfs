<?php

use Illuminate\Database\Seeder;

class testCustomersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('test_customers')->insert([
        [
            'customerNumber' => '1001',
            'customerName' => 'Dealban Info Sol',
            'address' => '11 Watermeyer Drive, Belvedere, Harare',
            'vatNo' => '10090009',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ],
        [
            'customerNumber' => '1002',
            'customerName' => 'Bluedot SMS',
            'address' => 'Avondale, Harare, Parks',
            'vatNo' => '120028800',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ],
        [
            'customerNumber' => '1003',
            'customerName' => 'Dairibord',
            'address' => 'Gerge W Bush Drive, Harare, Sparks Drive',
            'vatNo' => '10921100',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]
        ]);
    }
}
