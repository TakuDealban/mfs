<?php

use Illuminate\Database\Seeder;

class testProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('test_products')->insert([

            [
                'prodCode' => '00010',
                'prodDesc' => 'Matemba Test',
                'prod_till_desc' => 'Till Matemba Test',
                'prodQty' => 10,
                'prodPrice' => 8,
                'taxid' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'prodCode' => '00020',
                'prodDesc' => 'Huku Test',
                'prod_till_desc' => 'Till Huku Test',
                'prodQty' => 20,
                'prodPrice' => 19,
                'taxid' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'prodCode' => '00030',
                'prodDesc' => 'Mvura Test',
                'prod_till_desc' => 'Till Mvura Test',
                'prodQty' => 30,
                'prodPrice' => 10,
                'taxid' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]

        ]);
    }
}
