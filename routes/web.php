<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
    route::get('quotes', ['as' => 'quotes.index', 'uses' => 'quotesController@index']);
    Route::get('quotes/create', ['as' => 'quotes.create', 'uses' => 'quotesController@create']);
    Route::get('quotes/{ref}/show', ['as' => 'quotes.show', 'uses' => 'quotesController@show']);
    // Route::post('/select2/products/',['as' => 'quotes.getprods' , 'uses' => 'TestProductsController@showProducts']);
    Route::post('/select2/products/',['as' => 'quotes.getprods' , 'uses' => 'productsController@showProducts']);
   // Route::get('/autocomplete/customers/',['as' => 'getcustomers' , 'uses' => 'TestCustomersController@listCustomers']);
    Route::get('/autocomplete/customers/',['as' => 'getcustomers' , 'uses' => 'customerMaster@listCust']);
    Route::get('/customer_details',['as' => 'cust_info' , 'uses' => 'TestCustomersController@customer_info']); //
    Route::get('/customer_details',['as' => 'cust_info' , 'uses' => 'customerMaster@main_customer_info']);
    Route::post('/quotes/save',['as' => 'createquote', 'uses' => 'quotesController@store']);

    Route::get('pdfview/{ref}',array('as'=>'pdfview','uses'=>'quotesController@downloadQuote'));

    Route::get('/select2/search/',['as' => 'req.send.info', 'uses' => 'productsController@search']);

    Route::get('/select2/get-prod-info/{prod_code}',['as' => 'req.info', 'uses' => 'productsController@prod_info']);


    Route::get('/invoice/search/',['as' => 'invoice.search', 'uses' => 'invoicesController@index']);

     Route::post('/invoice/search_post/',['as' => 'invoice.post', 'uses' => 'invoicesController@show_invoce']);

     Route::get('/invoice/print/inv_num/{invoice_num}/cashier_no/{cashier_no}/tran_date/{tran_date}','invoicesController@print_invoice');
});

// Route::get('/autocomplete/customers/',['as' => 'getcustomers' , 'uses' => 'customerMaster@listCust']);

