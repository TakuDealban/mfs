<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteHeader extends Model
{
    //
    protected $table = 'TBLQUOTEHEADER';

    protected $fillable = [
        'QUOTENO', 'CUSTOMERNUMBER', 'CUSTOMERNAME','SUBTOTAL',
        'VATTOTAL','QOUTETOTAL'
    ];

   // protected $dateFormat = 'Y-m-d H:i';

    // protected $casts = [
    //     'CREATEDDATE' => 'datetime',
    // ];

}
