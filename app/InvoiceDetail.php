<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetail extends Model
{
    //
    protected $table = 'POSTRANITEM';

    public function hasInvProd(){
        return $this->hasOne('App\products','IDKEY','PRODPDCID');
    }
}
