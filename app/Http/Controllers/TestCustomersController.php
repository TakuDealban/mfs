<?php

namespace App\Http\Controllers;

use App\testCustomers;
use Illuminate\Http\Request;

class TestCustomersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\testCustomers  $testCustomers
     * @return \Illuminate\Http\Response
     */
    public function show(testCustomers $testCustomers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\testCustomers  $testCustomers
     * @return \Illuminate\Http\Response
     */
    public function edit(testCustomers $testCustomers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\testCustomers  $testCustomers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, testCustomers $testCustomers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\testCustomers  $testCustomers
     * @return \Illuminate\Http\Response
     */
    public function destroy(testCustomers $testCustomers)
    {
        //
    }

    public function listCustomers(Request $request)
    {
            $search = $request->cust_req;

            $customers = testCustomers::orderby('customerName','asc')->select('customerName','customerNumber', 'id')
                                                                    ->where('customerName', 'like', '%' .$search . '%')
                                                                     ->orWhere('customerNumber', 'like', '%' .$search . '%')
                                                                    ->limit(5)->get();

                                                                    $data = array();
                                                                    foreach($customers as $cust)
                                                                    {
                                                                        $data[] = array(
                                                                            "customInfo" => $cust["customerName"]."-".$cust["customerNumber"],
                                                                            "id" => $cust["id"]
                                                                        );

                                                                    }

                                                                    return response()->json($data);

        }

        public function customer_info(Request $request){
                    $cust_num = $request->customer_number;


                   $custInfo = testCustomers::where('customerNumber',$cust_num)->first();

                   if(empty($custInfo) || empty($cust_num))
                   {
                $custInfo['address'] = "New Account";
                $custInfo['customerNumber'] = '900900';
                $custInfo['vatNo'] = 'Cash Account';
                $custInfo['customerName'] = 'Cash Account';

                   }



                   return response()->json(["CustInfo" => $custInfo]);

        }
}
