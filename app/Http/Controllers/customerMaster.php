<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\custMaster;

class customerMaster extends Controller
{
    public function listCust(Request $request){
        $search = $request->cust_req;

        $customers = custMaster::orderby('NAME','asc')->select('NAME','USERCODE', 'IDKEY')
                                                                ->where('NAME', 'like', '%' .$search . '%')
                                                                 ->orWhere('USERCODE', 'like', '%' .$search . '%')
                                                                // ->limit(5)
                                                                ->get();

                                                                $data = array();
                                                                foreach($customers as $cust)
                                                                {
                                                                    $data[] = array(
                                                                        "customInfo" => $cust["NAME"]."-".$cust["USERCODE"],
                                                                        "id" => $cust["id"]
                                                                    );

                                                                }

                                                                return response()->json($data);

        }


        public function main_customer_info(Request $request)
        {
            $cust_num = $request->customer_number;

           $custInfo = custMaster::where('USERCODE',$cust_num)->first();

           $custData = array();
           if(empty($custInfo) || empty($cust_num))
           {
        $custData['address'] = "New Account";
        $custData['customerNumber'] = '900900';
        $custData['vatNo'] = 'Cash Account';
        $custData['customerName'] = 'Cash Account';

           }
           else{
               if($custInfo["ADDRESS1"] == "" || $custInfo["ADDRESS1"] == NULL){
                   $Addr1 = "No-Addr1";
               }
               else{
                $Addr1 = $custInfo["ADDRESS1"];
               }

               if($custInfo["ADDRESS2"] == "" || $custInfo["ADDRESS2"] == NULL){
                $Addr2 = "No-Addr2";
            }
            else{
                $Addr2 = $custInfo["ADDRESS2"];
               }

            if($custInfo["ADDRESS3"] == "" || $custInfo["ADDRESS3"] == NULL){
                $Addr3 = "No-Addr3";
            }
            else{
                $Addr3 = $custInfo["ADDRESS3"];
               }

            $custData["address"] = $Addr1." , ".$Addr2." , ".$Addr3;
            $custData['customerNumber'] =  $custInfo["USERCODE"];
            $custData['vatNo'] =  $custInfo["TAXREFNO"];
            $custData['customerName'] =  $custInfo["NAME"];
           // dd($custInfo);
           }



           return response()->json(['CustInfo' => $custData]);

}



}
