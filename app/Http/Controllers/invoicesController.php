<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\InvoiceDetail;
use App\branchmast;
use App\invoicesearchdetails;
use PDF;

class invoicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('invoice.invoice-search');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function show_invoce(Request $request)
    {
            $this->validate($request,[
                'invoice_number' => 'required|integer|min:1',
                'invoice_date' => 'required|date',
                'cashier_num' => 'required|integer'
            ]);
            // dd($request);

            $customer_vat = $request->input('customer_vat');
            if($customer_vat == null){
                $customer_vat = "No Vat Number";
            }

        $main_addr = $request->input('customer_addr');
        $newaddr = explode (",", $main_addr);
       // dd(count($newaddr));
        if(count($newaddr) == 1)
        {
            $addr_one = $newaddr[0] == NULL ? 'No_addr' : $newaddr[0];
            $addr_two = 'No_addr';
            $addr_three ='No_addr';
        }
        else if(count($newaddr) == 2){
            $addr_one = $newaddr[0] == NULL ? 'No_addr' : $newaddr[0];
            $addr_two = $newaddr[1] == NULL ? 'No_addr' : $newaddr[1];
        $addr_three = 'No_addr';

        }
        else{
            $addr_one = $newaddr[0] == NULL ? 'No_addr' : $newaddr[0];
            $addr_two = $newaddr[1] == NULL ? 'No_addr' : $newaddr[1];
        $addr_three = $newaddr[2] == NULL ? 'No_addr' : $newaddr[2];
        }





        $customer_number = $request->input('customer_number');
        $customer_name = $request->input('customer_name');
        $order_number = $request->input('order_number');

        if($order_number == null)
        {
            $order_number = 'No Order #';
        }

            // if($request->in)

         $invoice_header = Invoice::where('CASHIERNO',$request->input('cashier_num'))
                                    ->where('TRANNO',$request->input('invoice_number'))
                                    ->where('TRANDATE',$request->input('invoice_date'))
                                    ->first();



                if(empty($invoice_header)){
                    return redirect()->back()->withStatus(__('Invoice Not found for required match details.'));
                }
                else{
                    //save customer details
                    $savecustdate = invoicesearchdetails::updateOrCreate(
                        ['INVOICENUMBER' => $request->input('invoice_number'), 'CASHIERNUMBER' => $request->input('cashier_num'), 'TRANDATE' => $request->input('invoice_date') ],
                        ['CUSTOMERNUMBER' => $customer_number,
                        'CUSTOMERNAME' => $customer_name,
                        'ADDRESS1' => $addr_one,
                        'ADDRESS2' => $addr_two,
                        'ADDRESS3' => $addr_three,
                        'VATNUMBER' => $customer_vat,
                        'ORDERNUMBER' => $order_number,
                        ]
                    );

                    $header_id = $invoice_header->IDKEY;
                    $invoice_details = InvoiceDetail::where('HEADERID',$header_id)->get();
                    $branchcode = config('app.BRANCH_CODE');

                    $vat_total = 0;
                    foreach($invoice_details as $data){
                    $vat_amt = $data["TAXVALUE"];
                    $vat_total = $vat_total + $vat_amt;
                    }

                    $store_info = branchmast::where('USERCODE',$branchcode)->first();
                    // dd($store_info);
                     return view('invoice.invoice-info')->with(compact('invoice_header','invoice_details','store_info',
                                                                    'addr_one','addr_two','addr_three','order_number',
                                                                'customer_number','customer_name','customer_vat','vat_total'));
                }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function print_invoice($inv_num,$cashier_no,$tran_date)
    {
        // dd("Invoice : $inv_num, tran_date : $tran_date, casher number : $cashier_no");

        $invoice_header = Invoice::where('CASHIERNO',$cashier_no)
        ->where('TRANNO',$inv_num)
        ->where('TRANDATE',$tran_date)
        ->first();

        $header_id = $invoice_header->IDKEY;
        $invoice_details = InvoiceDetail::where('HEADERID',$header_id)->get();
        $branchcode = config('app.BRANCH_CODE');

        $vat_total = 0;
        foreach($invoice_details as $data){
        $vat_amt = $data["TAXVALUE"];
        $vat_total = $vat_total + $vat_amt;
        }

        $invoice_params = invoicesearchdetails::where('INVOICENUMBER',$inv_num)
                                                ->where('CASHIERNUMBER',$cashier_no)
                                                ->where('TRANDATE',$tran_date)
                                                ->first();

        $addr_one = $invoice_params->ADDRESS1;
        $addr_two = $invoice_params->ADDRESS2;
        $addr_three = $invoice_params->ADDRESS3;
        $order_number = $invoice_params->ORDERNUMBER;
        $customer_number = $invoice_params->CUSTOMERNUMBER;
        $customer_name = $invoice_params->CUSTOMERNAME;
        $customer_vat = $invoice_params->VATNUMBER;


        // dd($invoice_params);
        $store_info = branchmast::where('USERCODE',$branchcode)->first();
        // dd($store_info);

        //PDF

        $pdf = PDF::loadView('invoice.print-invoice',compact('invoice_header','invoice_details','store_info',
                                                        'addr_one','addr_two','addr_three','order_number',
                                                    'customer_number','customer_name','customer_vat','vat_total'))
                                        ->setPaper('a4')->setOrientation('portrait')->setOption('margin-top', 0);
         return $pdf->download('invoice_'.$cashier_no.'_'.$inv_num.'.pdf');

        //  return view('invoice.print-invoice')->with(compact('invoice_header','invoice_details','store_info',
        //                                                 'addr_one','addr_two','addr_three','order_number',
        //                                             'customer_number','customer_name','customer_vat','vat_total'));


    }



}
