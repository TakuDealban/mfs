<?php

namespace App\Http\Controllers;

use App\testProducts;
use Illuminate\Http\Request;

class TestProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\testProducts  $testProducts
     * @return \Illuminate\Http\Response
     */
    public function show(testProducts $testProducts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\testProducts  $testProducts
     * @return \Illuminate\Http\Response
     */
    public function edit(testProducts $testProducts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\testProducts  $testProducts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, testProducts $testProducts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\testProducts  $testProducts
     * @return \Illuminate\Http\Response
     */
    public function destroy(testProducts $testProducts)
    {
        //
    }

    public function showProducts(Request $request){
        $search = $request->search;

        if($search == ''){
           $testProducts = testProducts::orderby('prodDesc','asc')->select('id','prodCode','prodDesc')->limit(5)->get();
        }else{
           $testProducts = testProducts::orderby('prodDesc','asc')->select('id','prodCode','prodDesc')
                                                                    ->where('prodDesc', 'like', '%' .$search . '%')
                                                                    ->orWhere('prodCode', 'like', '%' .$search . '%')
                                                                    ->limit(5)->get();
        }

        $response = array();
        foreach($testProducts as $testProduct){
           $response[] = array(
                "id"=>$testProduct->id,
                "text"=>$testProduct->prodDesc
           );
        }

        echo json_encode($response);
        exit;
    }
}
