<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;
use App\QuoteHeader;
use App\QuoteItems;
use App\branchmast;
use PDF;


class quotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Quotes = QuoteHeader::all();
        return view('quotes.index')->with(compact('Quotes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = products::take(500)->get();

        return view('quotes.create')->with(compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'cust_number' => 'required',
        ]);

            /*
             * create header for quote
             * get a loop of all items and save the details
             * (***** VALIDATION ************)
             * Customer number
             * prod code
             * qty
             *
            */

        $products = $request->input('products');
        $customer_number = $request->input('cust_number');
        $customer_name = $request->input('cust_name');
        $customer_vat = $request->input('vat_no');
        $main_addr = $request->input('address');
        $quote_total = $request->input('quote_total');
        $vat_total = $request->input('vat_total');
        $sub_total = $request->input('sub_total');
        $quote_status = 'Valid';

        $last_insert_id = QuoteHeader::orderby('id','desc')->first();

        if(empty($last_insert_id)){
            $LastID = 0;
        }
        else{
            $LastID = $last_insert_id->id;
        }

        $next_index = $LastID + 1;
        $leading_zeros =  sprintf( '%08d', $next_index );
        $quote_no = env('BRANCH_CODE').'QUO'.$leading_zeros;


        //fix address
        $newaddr = explode (",", $main_addr);



        if($main_addr != 'New Account'){
            $addr_one = $newaddr[0] == NULL ? 'No_addr' : $newaddr[0];
            $addr_two = $newaddr[1] == NULL ? 'No_addr' : $newaddr[1];
            $addr_three = $newaddr[2] == NULL ? 'No_addr' : $newaddr[2];
        }
        else{
            $addr_one = 'No_addr';
            $addr_two = 'No_addr';
            $addr_three = 'No_addr';
        }


           /*********************CREATE QUOTE HEADER ********************/
        $new_header = new QuoteHeader();
        $new_header->QUOTENO = $quote_no;
        $new_header->CUSTOMERNUMBER = $customer_number;
        $new_header->CUSTOMERNAME = $customer_name;
        $new_header->VATNUMBER = $customer_vat;
        $new_header->ADDRESS1 = $addr_one;
        $new_header->ADDRESS2 = $addr_two;
        $new_header->ADDRESS3 = $addr_three;
        // $new_header->SUBTOTAL = $sub_total;
        // $new_header->VATTOTAL = $vat_total;
        // $new_header->QOUTETOTAL = $quote_total;
        $new_header->QUOTESTATUS = $quote_status;
        $new_header->CREATEDBY = auth()->user()->id;
        $new_header->CREATEDDATE = date("Y-m-d H:i:s");
        $new_header->save();

        /********Create quote items */
        $qouteHeaderId = $new_header->id;

        $quote_total = 0;
        $sub_total = 0;
        $total_vat = 0;

        foreach($products as $prod)
        {
        $new_quote_item = new QuoteItems();
        $new_quote_item->USERCODE = $prod["prod_code"];
        $new_quote_item->ORDERQTY = $prod["order_qty"];
        $new_quote_item->PRICE = $prod["price"];
        $new_quote_item->LINEVAT = $prod["line_vat"];
        $new_quote_item->LINETOTAL = $prod["line_tot"];
        $new_quote_item->QUOTEREFID = $qouteHeaderId;
        $new_quote_item->CREATEDBY = auth()->user()->id;
        $new_quote_item->CREATEDDATE = date("Y-m-d H:i:s");
        $new_quote_item->save();

        $quote_total = $quote_total + $prod["line_tot"];
        $total_vat = $total_vat + $prod["line_vat"];

        }

        $sub_total = $quote_total - $total_vat;

        $update_header = QuoteHeader::where('id',$qouteHeaderId)->update(array(
            'SUBTOTAL' => round($sub_total,2),
            'QOUTETOTAL' => round($quote_total,2),
            'VATTOTAL' => round($total_vat,2)
        ));
        // update header


         return response()->json(array("success" => true,
                                     "request" => $request),$status = 200);

        //return response()->json(array("request" => $request));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


       $Quote_header = QuoteHeader::where('QUOTENO',$id)->first();
       $quote_id = $Quote_header->id;
       $quote_total = $Quote_header->QOUTETOTAL;
       $quote_vat_total = $Quote_header->VATTOTAL;
       $subtotal = $Quote_header->SUBTOTAL;
       $Quote_items = QuoteItems::where('QUOTEREFID',$quote_id)->get();
       $store_info = branchmast::where('USERCODE',config('app.BRANCH_CODE'))->first();
       $ip_address = \request()->ip();

         return view('quotes.quote_info')->with(compact('ip_address','Quote_items','quote_total','quote_vat_total','subtotal','Quote_header','store_info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function downloadQuote($id)
    {



        $Quote_header = QuoteHeader::where('QUOTENO',$id)->first();
        $quote_id = $Quote_header->id;
        $quote_total = $Quote_header->QOUTETOTAL;
        $quote_vat_total = $Quote_header->VATTOTAL;
        $subtotal = $Quote_header->SUBTOTAL;
        $Quote_items = QuoteItems::where('QUOTEREFID',$quote_id)->get();

        $first_print = $Quote_header->print_number;
        $first_print_date = $Quote_header->first_print;

        if($first_print_date == null)
        {
            $first_print_date = date("Y-m-d H:i:s");
        }

        if($first_print == NULL){
            $first_print = 0;

        }

        $next_print = $first_print + 1;

        //update print info
        QuoteHeader::where('QUOTENO',$id)->update(
            array(
            'first_print' => $first_print_date,
            'print_number' => $next_print,
            'current_print_date' => date("Y-m-d H:i:s"),
            'curr_print_by' => auth()->user()->id

        ));

        $store_info = branchmast::where('USERCODE',env('BRANCH_CODE'))->first();


        $pdf = PDF::loadView('quotes.downloadquote',compact('Quote_items','quote_total','quote_vat_total','subtotal','Quote_header','store_info'))
        ->setPaper('a4')->setOrientation('portrait')->setOption('margin-top', 0);
         return $pdf->download('Quote_'.$id.'_'.$next_print.'.pdf');

        // return view('quotes.downloadquote')->with(compact('Quote_items','quote_total','quote_vat_total','subtotal','Quote_header'));


      }
}
