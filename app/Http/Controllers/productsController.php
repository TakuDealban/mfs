<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;

class productsController extends Controller
{

    public function showProducts(Request $request){
        $search = $request->search;

        if($search == ''){
           $products = products::orderby('USERCODE','asc')->select('IDKEY','USERCODE','NAME')->limit(5)->get();
        }else{
           $products = products::orderby('NAME','asc')->select('IDKEY','USERCODE','NAME')
                                                                    ->where('NAME', 'like', '%' .$search . '%')
                                                                    ->orWhere('USERCODE', 'like', '%' .$search . '%')
                                                                    ->limit(5)->get();
        }

        $response = array();
        foreach($products as $testProduct){
           $response[] = array(
                "id"=>$testProduct->id,
                "text"=>$testProduct->prodDesc
           );
        }

        echo json_encode($response);
        exit;
    }

    public function search(Request $request)
    {
        $products = products::where('NAME', 'LIKE', '%'.$request->input('term', '').'%')
                            ->orWhere('USERCODE', 'like', '%' .$request->input('term', '') . '%')
                            ->get();


            $response = array();
            foreach($products as $testProduct)
            {
                $stock_on_hand = $testProduct->STOCKONHAND - $testProduct->STOCKALLOC;
                $response[] = array(
                     "id"=>$testProduct->USERCODE,
                     "text"=>$testProduct->NAME.' - '.$testProduct->USERCODE.' -> In Stock = '.$stock_on_hand
                );
             }
        return ['results' => $response];

    }

    public function prod_info($prod_code){
           // $prod_code = $request->id;
            $product = products::Where('USERCODE', '=', $prod_code)
                            ->first();

            $stock_on_hand = $product->STOCKONHAND - $product->STOCKALLOC;
            $proddesc = $product->NAME;
            $price = $product->SELLPRICE;
            // $instock= $stock_on_hand;
            $taxcode = $product->hasVatCode->TAXID;
            $till_desc = $product->TILLDESC;

                $response['tax_code'] = $product->hasVatCode->TAXID;
                $response['stock_on_hand'] = (float)$stock_on_hand;
                $response['proddesc'] = $proddesc;
                $response['price'] = $price;
                $response['taxcode'] = $taxcode;
                $response['till_desc'] = $till_desc;

                return ['results' => $response];
    }

}
