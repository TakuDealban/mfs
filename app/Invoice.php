<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
    protected $table = 'POSTRANHEADER';


    public function hasCashName(){
        return $this->hasOne('App\cashiermaster','CASHIERNO','CASHIERNO');
    }
}
