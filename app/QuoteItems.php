<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteItems extends Model
{
    //
    protected $table = 'TBLQUOTEDETAILS';

    protected $fillable = [
        'ORDERQTY', 'PRICE', 'USERCODE','LINEVAT',
        'LINETOTAL','QUOTEREFID'
    ];

    // protected $casts = [
    //     'CREATEDDATE' => 'datetime',
    // ];

    public function hasProd(){
        return $this->hasOne('App\products','USERCODE','USERCODE');
    }
}
