<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class invoicesearchdetails extends Model
{
    //
    protected $table = 'TBLINVOICESEARCHPARAMS';

    protected $fillable = ['INVOICENUMBER' , 'CASHIERNUMBER', 'TRANDATE' ,'CUSTOMERNAME','CUSTOMERNUMBER',
                            'ADDRESS1' ,'ADDRESS2', 'ADDRESS3', 'VATNUMBER','ORDERNUMBER',
                            ];
}
