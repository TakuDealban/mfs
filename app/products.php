<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class products extends Model
{
    //
    protected $table = 'PRODPDC';

    public function hasVatCode(){
        return $this->hasOne('App\productsVAT','USERCODE', 'USERCODE');
    }
}
