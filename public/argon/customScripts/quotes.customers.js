$(document).ready(function(){

var path = '/autocomplete/customers/';

        $(".typeahead").typeahead({
            items: 4,
            source: function(request, response) {
                $.ajax({
                    url:path,
                    dataType: "json",
                    data: {
                      //  state: $("#StateID").val(),
                        term: request.term
                    },
                    success: function (data) {
                        response(data);


                    }
                });
            },
            autoSelect: true,
            displayText: function (item) {
                return item.customInfo;
              //  $(".typeahead").trigger('selected', {"id": item.id, "value": item.customInfo});

            }
        });

        $("#customerData").on('change',function(ev){
            ev.preventDefault();
            var in_text = $(this).val();
            var cus_nm = in_text.slice(in_text.length - 6);

           $.ajax({
            url:'/customer_details',
            dataType: "json",
            method:'get',
            data: {
              //  state: $("#StateID").val(),
                customer_number: cus_nm
            },
            success: function (data) {

               console.log(data);
                $("#customerAddr").val(data.CustInfo.address);
                $("#customerNumero").val(data.CustInfo.customerNumber);
                $("#custVatNo").val(data.CustInfo.vatNo);
                $("#custName").val(data.CustInfo.customerName);


            },
            error: function (data){
                $("#customerAddr").val("Into yami");
            }
        });



        });

});
