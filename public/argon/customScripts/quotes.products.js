

   // CSRF Token

   $(document).ready(function(){

    // $( "#selProduct" ).select2();

    // update field infor
    // $( "#selProduct" ).on('change',function(ev){
    //     ev.preventDefault();
    //     let id = $(this).val();
    //     var prodDesc = $(this).children('option:selected').data('proddesc');
    //     var prodCode = $(this).children('option:selected').data('prodcode');
    //     var unitPrice = parseFloat($(this).children('option:selected').data('price')).toFixed(2);
    //     var instock = $(this).children('option:selected').data('instock');
    //     var taxcode = $(this).children('option:selected').data('taxcode');
    //     var till_desc = $(this).children('option:selected').data('till_desc');


    //     //alert(instock);
    //     $("#unitPrice").val(unitPrice);
    //     $("#inStock").val(instock);
    //     $("#prod_desc").val(prodDesc);
    //     $("#prod_code").val(prodCode);
    //     $("#taxcode").val(taxcode);
    //     $("#curr_id").val(id);
    //     $("#till_desc").val(till_desc);
    //     //  alert(prodDesc);
    // });

    $('#product_select').select2({
        minimumInputLength: 3,
        placeholder: "Select a Product",
        ajax: {
            url: '/select2/search/',
            dataType: 'json',
        },
    });

    $('#product_select').on('change',function(ev){
        ev.preventDefault();
        let id = $(this).val();
        // alert(id);

        //var APP_URL = $('meta[name='"_base_url"]').attr('content');
        var APP_URL = $('meta[name="_base_url"]').attr('content');
        $.ajax({url: APP_URL+"/select2/get-prod-info/"+id,
        type:'GET',
        dataType:'json',
         success: function(results){
       console.log(results);

       console.log(results.results.stock_on_hand);


        var prodDesc = results.results.proddesc;
        var prodCode = id;
        var unitPrice = parseFloat(results.results.price).toFixed(2);
        var instock = parseFloat(results.results.stock_on_hand).toFixed(2);
        var taxcode = results.results.taxcode;
        var till_desc = results.results.till_desc;


        $("#unitPrice").val(unitPrice);
        $("#inStock").val(instock);
        $("#prod_desc").val(prodDesc);
        $("#prod_code").val(prodCode);
        $("#taxcode").val(taxcode);
        $("#curr_id").val(id);
        $("#till_desc").val(till_desc);

          }});
    });



    $("#prodUploadContentTbl").hide();
    $(".btn_create_quote").hide();

    //on buttonn upload line
    var sum = 0;
    var vat_total = 0;
    var qty_tot = 0;

    $("#btn_add_new_quote_line").on('click',function(ev)
    {
        ev.preventDefault();
        var qty = parseInt($("#order_qty").val());
        var prod_code =  $("#prod_code").val();
        var curr_prod_id = $("#curr_id").val();
        var in_stock = $("#inStock").val();
        $(".btn_create_quote").show('slow');
        if(prod_code == null || prod_code == "")
        {
alert("No product has been selected");
        }

        else if(qty > 0){
            var prod_desc =  $("#prod_desc").val();

            var after_tax_price =  $("#unitPrice").val();
            var tax_code = parseInt($("#taxcode").val());
            var till_desc = $("#till_desc").val();

            if(tax_code === 1){
                var price_before_tax = parseFloat(after_tax_price / 1.15).toFixed(2);
            }
            else{
                var price_before_tax = after_tax_price
            }

            var line_tax = after_tax_price - price_before_tax;

            var vat_line_total = parseFloat(qty * line_tax).toFixed(2);
            var line_total = parseFloat(qty * after_tax_price).toFixed(2);

            sum = sum + parseFloat(line_total);
            vat_total = vat_total + parseFloat(vat_line_total);
            qty_tot = qty_tot + parseFloat(qty);
            $("#prodUploadContentTbl").show('slow');
        //load to ui -
var new_row = $('<tr id="'+curr_prod_id+'">'+
        '<td scope="row"><div class="media align-items-center"><div class="media-body">'+
            '<span class="mb-0 text-sm">'+prod_code+'</span> </div></div></td>'+

        '<td>'+prod_desc+'</td>'+
        '<td>'+till_desc+'</td>'+
        '<td class="text-right"> '+qty+'</td>'+
        '<td class="text-right">'+after_tax_price+'</td>'+
        '<td class="text-right">'+vat_line_total+'</td>'+
        '<td class="text-right">'+line_total+'</td>'+
        '<td class="text-right">'+

                '<a class="btn btn-sm btn-danger btn_delete_line" data-toggle="tooltip" title="remove item">'+
                '<input type="hidden" class="prod_queue" in_stock="'+in_stock+'" till_desc="'+till_desc+'" prod_code="'+prod_code+'" prod_desc="'+prod_desc+'" price="'+after_tax_price+'" tax_code="'+tax_code+'" qty="'+qty+'" vat_line_total="'+vat_line_total+'" line_tot="'+line_total+'"">' +
                  '<i class="ni ni-fat-remove"></i></a>'+
            '</td>'+

        '</tr>');

            $(".tbody_cls").append(new_row);

            //remove sku from listing
            $("#product_select option[value='"+curr_prod_id+"']").remove();

            //line totals at the end of the table
            $(".vat_tot").html('<strong class="h3">'+vat_total.toFixed(2)+'</strong>');
            $(".qty_tot").html('<strong class="h3">'+qty_tot.toFixed(2)+'</strong>');
            $(".quote_tot").html('<strong class="h3">'+sum.toFixed(2)+'</strong>');

            // reset fields for a new value set
            $("#order_qty").val('');
            $("#unitPrice").val(0);
            $("#inStock").val(0);
            $("#prod_code").val("")

        }
        else{
            alert("Order Quantity must be greater than zero");
        }


    });

    $("table.uploadProds ").on('click','.btn_delete_line',function(ev){
    ev.preventDefault();
    //alert("clicked");
var pro_id = $(this).closest('tr').attr('id');
var prod_desc = $(this).find('.prod_queue').attr('prod_desc');
var prod_code = $(this).find('.prod_queue').attr('prod_code');
var price = $(this).find('.prod_queue').attr('price');//tax_code
var tax_code = $(this).find('.prod_queue').attr('tax_code');//tax_code
var line_tot = $(this).find('.prod_queue').attr('line_tot');
var vat_line_total = $(this).find('.prod_queue').attr('vat_line_total');
var qty = $(this).find('.prod_queue').attr('qty');
var instock = $(this).find('.prod_queue').attr('in_stock');
var till_desc = $(this).find('.prod_queue').attr('till_desc');

sum = sum - parseFloat(line_tot);
vat_total = vat_total - parseFloat(vat_line_total);
qty_tot = qty_tot - parseFloat(qty);

$(this).closest('tr').remove();
$(".vat_tot").html('<strong class="h3">'+vat_total.toFixed(2)+'</strong>');
$(".qty_tot").html('<strong class="h3">'+qty_tot.toFixed(2)+'</strong>');
$(".quote_tot").html('<strong class="h3">'+sum.toFixed(2)+'</strong>');


// $("#selProduct").append('<option value="'+pro_id+'" data-proddesc="'+prod_desc+'"'+
//         'data-prodcode = "'+prod_code+'" data-price="'+price+'" data-instock="'+instock+'"'+
//         'data-taxcode="'+tax_code+'"' +
//         'data-till_desc="'+till_desc+'">' +
//         prod_desc + ' - (' + prod_code + ')</option>');
    });


   });

