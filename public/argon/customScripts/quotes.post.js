$(document).ready(function(){

    $(".response").hide();
    var baseUrl = $('meta[name="_base_url"]').attr('content');
$('.btn_create_quote').click(function(ev){
    ev.preventDefault();

    //message to for client to wait
    //disable button until response is processed

    $(".response").html("<div class='alert alert-info'>... Processing quotation, please wait...");

    $(".response").slideDown("slow");

    var cust_number = $("#customerNumero").val();
    var cust_name = $("#custName").val();
    var vat_no =  $("#custVatNo").val();
    var address = $("#customerAddr").val();

    //create a loop of products
    var products = [];
    var quote_total = 0;
    var vat_total = 0;
    var sub_total = 0;


    function FormField(){
        this.prod_code;
        this.price;
        this.order_qty;
        this.line_vat;
        this.line_total;
    }

    $.each($("table.uploadProds").find('.prod_queue'),
    function(index,value){
        var fl = new FormField();
        fl.prod_code = $(value).attr('prod_code');
        fl.order_qty = $(value).attr('qty');
        fl.price = $(value).attr('price');
        fl.line_vat = $(value).attr('vat_line_total');
        fl.line_tot = parseFloat($(value).attr('line_tot'));
        // quote_total = parseFloat(quote_total + fl.line_tot).toFixed(2);
        // vat_total = parseFloat(vat_total + fl.line_vat).toFixed(2);
        //console.log(fl);
        products.push(fl);


    });

    //console.log(products);
    sub_total = parseFloat(quote_total - vat_total).toFixed(2);

    var formData = {
        products:products,
        quote_total:quote_total,
        vat_total:vat_total,
        sub_total:sub_total,
        cust_number: cust_number,
        cust_name:cust_name,
        vat_no:vat_no,
        address:address
    }

   // console.log(formData);

    if(products.length > 0){

        $.ajax({
        headers:{
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type:'POST',
        url: "/quotes/save",
        data:formData,
        dataType:'json',
        success:function(data){

            $(".response").html("<div class='alert alert-success'>Quotation has been created successfully, please wait as the system update changes</div>")
            $(".response").slideDown("slow");

            setTimeout(function(){
                window.location.replace(baseUrl + '/quotes');
            },3000);


        },
        error:function(data){
            console.log(data);
            alert(data.responseText);
        }

    });
}
else{
    alert('Please add products to the list');
}

});



});
